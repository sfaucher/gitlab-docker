## Description

Simple proof of concept for building and deploying Docker images to Gitlab Container Registry

## Installation

```bash
$ docker run --name gitlab -p 3000:3000 registry.gitlab.com/sfaucher/gitlab-docker:master
```
